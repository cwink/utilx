#ifndef UTILX_HPP
#define UTILX_HPP

#include <chrono>
#include <cmath>
#include <functional>
#include <random>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

namespace utilx {
constexpr auto pi = 3.14159265359;

using Error = std::runtime_error;

auto jenkins_hash(const std::string &key) noexcept -> std::uint32_t;

auto base64_encode(const std::int8_t *buffer, std::size_t size) noexcept -> std::string;
auto base64_encode(const std::uint8_t *buffer, std::size_t size) noexcept -> std::string;
auto base64_encode(const std::vector<std::int8_t> &buffer) noexcept -> std::string;
auto base64_encode(const std::vector<std::uint8_t> &buffer) noexcept -> std::string;
auto base64_decode(const std::string &string) -> std::vector<std::int8_t>;

template <typename Type, typename = std::enable_if_t<std::is_arithmetic_v<Type>>>
auto random_number(const Type minimum, const Type maximum) -> Type {
  auto random_device = std::random_device();
  auto mt = std::mt19937(random_device());

  if constexpr (std::is_integral_v<Type>) {
    auto distribution = std::uniform_int_distribution<Type>(minimum, maximum);
    return distribution(mt);
  } else { // NOLINT
    auto distribution = std::uniform_real_distribution<Type>(minimum, maximum);
    return distribution(mt);
  }
}

template <typename Type, typename = std::enable_if_t<std::is_floating_point_v<Type>>>
auto equal(const Type left, const Type right) -> bool {
  return std::fabs(left - right) < 0.0001;
}

auto degrees(double radians) noexcept -> double;
auto radians(double degrees) noexcept -> double;

auto ticks() -> double;

using Integrator = std::function<void(const double, const double)>;

class Timestep final {
  double time_{0.0};
  double delta_time_{0.01};
  double current_time_{ticks() * 1e-9};
  double accumulator_{0.0};

public:
  Timestep() = default;
  Timestep(const Timestep &) noexcept = delete;
  Timestep(Timestep &&) noexcept = default;
  auto operator=(const Timestep &) noexcept -> Timestep & = delete;
  auto operator=(Timestep &&) noexcept -> Timestep & = default;
  ~Timestep() noexcept = default;

  explicit Timestep(double delta_time);

  auto update() noexcept -> void;
  auto integrate(const Integrator &integrator) -> void;
};
} // namespace utilx

#endif // UTILX_HPP
