#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "include/utilx.hpp"

#include <iostream>

TEST_CASE("Main", "[main]") {
  std::cout << utilx::random_number(1, 10) << std::endl;
  std::cout << utilx::random_number(27.2, 30.5) << std::endl;
}

SCENARIO("When a jenkins hash string is generated, the value is correct.", "[jenkins_hash_gen]") {
  GIVEN("A hash string.") {
    constexpr auto string = "Lol I don't know.";

    WHEN("A hash is generated.") {
      const auto hash = utilx::jenkins_hash(string);

      THEN("The value is correct.") { REQUIRE(hash == 2798192711); }
    }
  }
}

SCENARIO("When a buffer is base64 encoded, the result is correct.", "[base64_encode]") {
  GIVEN("A few buffers.") {
    constexpr std::int8_t buffer1[] = "Mary had a little lamb.";
    constexpr std::uint8_t buffer2[] = "This is a test.";
    const auto buffer3 = std::vector<std::int8_t>({'A', 'B', 'C', 'D', 'E', 'F', 'G'});
    const auto buffer4 = std::vector<std::int8_t>({'1', '2', '3', 'D', 'E', 'F'});

    WHEN("Base64 encoded strings are generated.") {
      const auto base1 = utilx::base64_encode(buffer1, 23);
      const auto base2 = utilx::base64_encode(buffer2, 15);
      const auto base3 = utilx::base64_encode(buffer3.data(), buffer3.size());
      const auto base4 = utilx::base64_encode(buffer4);

      THEN("The result is correct.") {
        REQUIRE(base1 == "TWFyeSBoYWQgYSBsaXR0bGUgbGFtYi4=");
        REQUIRE(base2 == "VGhpcyBpcyBhIHRlc3Qu");
        REQUIRE(base3 == "QUJDREVGRw==");
        REQUIRE(base4 == "MTIzREVG");
      }
    }
  }
}

SCENARIO("When a string is base64 decoded, the result is correct.", "[base64_decode]") {
  GIVEN("A few strings.") {
    constexpr auto string1 = "TWFyeSBoYWQgYSBsaXR0bGUgbGFtYi4=";
    constexpr auto string2 = "VGhpcyBpcyBhIHRlc3Qu";
    constexpr auto string3 = "QUJDREVGRw==";
    constexpr auto string4 = "MTIzREVG";

    const auto result1 = std::vector<std::int8_t>({'M', 'a', 'r', 'y', ' ', 'h', 'a', 'd', ' ', 'a', ' ', 'l',
                                                   'i', 't', 't', 'l', 'e', ' ', 'l', 'a', 'm', 'b', '.'});
    const auto result2 =
        std::vector<std::int8_t>({'T', 'h', 'i', 's', ' ', 'i', 's', ' ', 'a', ' ', 't', 'e', 's', 't', '.'});
    const auto result3 = std::vector<std::int8_t>({'A', 'B', 'C', 'D', 'E', 'F', 'G'});
    const auto result4 = std::vector<std::int8_t>({'1', '2', '3', 'D', 'E', 'F'});

    WHEN("Base64 decoded buffers are generated.") {
      const auto buffer1 = utilx::base64_decode(string1);
      const auto buffer2 = utilx::base64_decode(string2);
      const auto buffer3 = utilx::base64_decode(string3);
      const auto buffer4 = utilx::base64_decode(string4);

      THEN("The result is correct.") {
        REQUIRE(buffer1 == result1);
        REQUIRE(buffer2 == result2);
        REQUIRE(buffer3 == result3);
        REQUIRE(buffer4 == result4);
      }
    }
  }
}

SCENARIO("When converting between degrees and radians, the result is correct.", "[degrees_radians]") {
  GIVEN("A degrees and radians conversion.") {
    const auto degrees = utilx::degrees(1.25);
    const auto radians = utilx::radians(179.0);

    WHEN("The values are tests.") {
      THEN("The results are correct.") {
        REQUIRE(utilx::equal(degrees, 71.6197));
        REQUIRE(utilx::equal(radians, 3.12414));
      }
    }
  }
}
