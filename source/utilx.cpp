#include "../include/utilx.hpp"

#include <algorithm>
#include <sstream>

namespace {
constexpr std::int8_t base64_index_table[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
    'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

constexpr std::uint8_t base64_char_table[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  62, 0,  0,  0,  63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,  0,  0,  26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
} // namespace

namespace utilx {
auto jenkins_hash(const std::string &key) noexcept -> std::uint32_t {
  auto hash = static_cast<std::uint32_t>(0);

  for (const auto value : key) {
    hash += static_cast<std::uint32_t>(value);
    hash += hash << 10; // NOLINT
    hash ^= hash >> 6;  // NOLINT
  }

  hash += hash << 3;  // NOLINT
  hash ^= hash >> 11; // NOLINT
  hash += hash << 15; // NOLINT

  return hash;
}

auto base64_encode(const std::int8_t *const buffer, const std::size_t size) noexcept -> std::string {
  auto string_stream = std::ostringstream();

  for (auto index = static_cast<std::size_t>(0); index < size; index += 3) {
    auto a = '=';
    auto b = '=';
    auto c = '=';
    auto d = '=';
    auto data = static_cast<std::uint32_t>(0);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wc++98-c++11-compat-binary-literal"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wc++98-c++11-compat-binary-literal"
    switch (size - index) {
    case 1:
      data = data | static_cast<std::uint32_t>(buffer[index]); // NOLINT
      data = data << 16;                                       // NOLINT

      a = base64_index_table[(data & 0b111111000000000000000000) >> 18]; // NOLINT
      b = base64_index_table[(data & 0b000000111111000000000000) >> 12]; // NOLINT
      break;
    case 2:
      data = data | static_cast<std::uint32_t>(buffer[index]);            // NOLINT
      data = (data << 8) | static_cast<std::uint32_t>(buffer[index + 1]); // NOLINT
      data = data << 8;                                                   // NOLINT

      a = base64_index_table[(data & 0b111111000000000000000000) >> 18]; // NOLINT
      b = base64_index_table[(data & 0b000000111111000000000000) >> 12]; // NOLINT
      c = base64_index_table[(data & 0b000000000000111111000000) >> 6];  // NOLINT
      break;
    default:
      data = data | static_cast<std::uint32_t>(buffer[index]);            // NOLINT
      data = (data << 8) | static_cast<std::uint32_t>(buffer[index + 1]); // NOLINT
      data = (data << 8) | static_cast<std::uint32_t>(buffer[index + 2]); // NOLINT

      a = base64_index_table[(data & 0b111111000000000000000000) >> 18]; // NOLINT
      b = base64_index_table[(data & 0b000000111111000000000000) >> 12]; // NOLINT
      c = base64_index_table[(data & 0b000000000000111111000000) >> 6];  // NOLINT
      d = base64_index_table[(data & 0b000000000000000000111111)];       // NOLINT
      break;
    }
#pragma GCC diagnostic pop
#pragma clang diagnostic pop

    string_stream << a << b << c << d;
  }

  return string_stream.str();
}

auto base64_encode(const std::uint8_t *const buffer, const std::size_t size) noexcept -> std::string {
  return base64_encode(reinterpret_cast<const std::int8_t *const>(buffer), size); // NOLINT
}

auto base64_encode(const std::vector<std::int8_t> &buffer) noexcept -> std::string {
  return base64_encode(buffer.data(), buffer.size());
}

auto base64_encode(const std::vector<std::uint8_t> &buffer) noexcept -> std::string {
  return base64_encode(buffer.data(), buffer.size());
}

auto base64_decode(const std::string &string) -> std::vector<std::int8_t> {
  if (string.size() % 4 != 0) {
    throw Error("Invalid string size.");
  }

  if (!std::all_of(std::cbegin(string), std::cend(string) - 2,
                   [](const auto ch) { return std::isalnum(ch) || ch == '+' || ch == '/'; })) {
    throw Error("Invalid characters found in string.");
  }

  if (!(std::isalnum(*(std::cend(string) - 2)) != 0 || *(std::cend(string) - 2) == '+' ||
        *(std::cend(string) - 2) == '/' || *(std::cend(string) - 2) == '=')) {
    throw Error("Invalid characters found in string.");
  }

  if (!(std::isalnum(*(std::cend(string) - 1)) != 0 || *(std::cend(string) - 1) == '+' ||
        *(std::cend(string) - 1) == '/' || *(std::cend(string) - 1) == '=')) {
    throw Error("Invalid characters found in string.");
  }

  auto buffer = std::vector<std::int8_t>();

  for (auto it = std::cbegin(string); it < std::cend(string); it += 4) {
    const auto a = base64_char_table[static_cast<std::int32_t>(*it)];       // NOLINT
    const auto b = base64_char_table[static_cast<std::int32_t>(*(it + 1))]; // NOLINT
    const auto c = base64_char_table[static_cast<std::int32_t>(*(it + 2))]; // NOLINT
    const auto d = base64_char_table[static_cast<std::int32_t>(*(it + 3))]; // NOLINT

    auto data = static_cast<std::uint32_t>(0);
    data = data | static_cast<std::uint32_t>(a << 18); // NOLINT
    data = data | static_cast<std::uint32_t>(b << 12); // NOLINT
    data = data | static_cast<std::uint32_t>(c << 6);  // NOLINT
    data = data | static_cast<std::uint32_t>(d);       // NOLINT

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wc++98-c++11-compat-binary-literal"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wc++98-c++11-compat-binary-literal"
    const auto A = static_cast<std::int8_t>((data >> 16) & 0b00000000000000000000000011111111); // NOLINT
    const auto B = static_cast<std::int8_t>((data >> 8) & 0b00000000000000000000000011111111);  // NOLINT
    const auto C = static_cast<std::int8_t>(data & 0b00000000000000000000000011111111);         // NOLINT
#pragma GCC diagnostic pop
#pragma clang diagnostic pop

    if (*(it + 2) == '=' && *(it + 3) == '=') {
      buffer.emplace_back(A);
    } else if (*(it + 3) == '=') {
      buffer.emplace_back(A);
      buffer.emplace_back(B);
    } else {
      buffer.emplace_back(A);
      buffer.emplace_back(B);
      buffer.emplace_back(C);
    }
  }

  return buffer;
}

auto degrees(const double radians) noexcept -> double { return radians * 180.0 / pi; }

auto radians(const double degrees) noexcept -> double { return degrees * pi / 180.0; }

auto ticks() -> double {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
             std::chrono::high_resolution_clock::now().time_since_epoch())
      .count();
}

Timestep::Timestep(const double delta_time) : delta_time_{delta_time} {}

auto Timestep::update() noexcept -> void {
  const auto new_time = ticks() * 1e-9;
  const auto frame_time = new_time - current_time_;
  current_time_ = new_time;
  accumulator_ += frame_time;
}

auto Timestep::integrate(const Integrator &integrator) -> void {
  while (accumulator_ >= delta_time_) {
    integrator(time_, delta_time_);

    accumulator_ -= delta_time_;
    time_ += delta_time_;
  }
}
} // namespace utilx
